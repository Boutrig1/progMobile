# Description du site

Ce site utilise une API, cette API gère des appels de fonctions et édite une base de donnée.
Nous avons utilisés VueJs, NodeJs, Pug, SCSS, pug  et Bootstrapvue pour le frontend et pour le backend nous avons utilisés JDBC, Maven, SQLite et Spring.

# Fonctionnalités du site

- Inscription 
- Connexion
- Supression de compte
- Changement de mail 
- Changement de mot de passe
- Creation de sondage
- Supression de sondage
- Vote sur les sondages

# Configuration

- L'API  utilise le port 8081
- Le site utilise le port 8080

# Déploiement du site 

Dans le dossier front
```bash
npm install
npm run serve
```

Dans le dossier back
```bash
mvnw spring-boot:run
```

# Contributeur du site
- Alexandre Boutrig

- Angelo Lebbolo
