package com.example.springboot;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class bdd {

   private Connection connect() {
      // SQLite connection string
      String url = "jdbc:sqlite:test.db";
      Connection conn = null;
      try {
          conn = DriverManager.getConnection(url);
      } catch (SQLException e) {
          System.out.println(e.getMessage());
          System.out.println(0);
      }
      return conn;
  }



    public bdd( ) {
        Connection c = null;
        Statement stmt = null;
        
       try {
           
           c = DriverManager.getConnection("jdbc:sqlite:test.db");
           System.out.println("Opened database successfully");
  
           stmt = c.createStatement();
           String CreateTableUser = "CREATE TABLE IF NOT EXISTS USER " +
                          "(ID INTEGER      NOT NULL ," +
                          " PSEUDO         TEXT    NOT NULL, " + 
                          " PASSWORD       TEXT    NOT NULL, " + 
                          " MAIL           TEXT     NOT NULL, "+
                          " PRIMARY KEY(\"ID\" AUTOINCREMENT)  )" ;

            String CreateTableAppointment = "CREATE TABLE IF NOT EXISTS APPOINTEMENT " +
                           "(ID INTEGER     NOT NULL   ," +
                           " LOCATION         TEXT    NOT NULL, " + 
                           " ARG1            TEXT    NOT NULL, " + 
                           " ARG2         TEXT    NOT NULL, " + 
                           " DATE           TEXT    NOT NULL, "+
                           "PRIMARY KEY(\"ID\" AUTOINCREMENT) )" ;
                           
            String CreateTableVote = "CREATE TABLE IF NOT EXISTS VOTE " +
                           "(ID             INTEGER     NOT NULL   ," +
                           " IDuser         INTEGER     NOT NULL, " + 
                           " IDappointement INTEGER    NOT NULL, "+
                           " Vote1 INTEGER    NOT NULL, "+
                           " Vote2 INTEGER    NOT NULL, "+
                           "FOREIGN KEY(IDappointement) REFERENCES appointement(id)," +
                           "FOREIGN KEY(Iduser) REFERENCES user(id)," +
                           "PRIMARY KEY(\"ID\" AUTOINCREMENT) )" ;


           stmt.executeUpdate(CreateTableAppointment);
           stmt.executeUpdate(CreateTableUser);
           stmt.executeUpdate(CreateTableVote);

           stmt.close();
           c.close();
        } catch ( Exception e ) {
           System.err.println( e.getClass().getName() + ": " + e.getMessage() );
           System.out.println(1);
           System.exit(0);
           
        }
        System.out.println("Table created successfully");
     }



         public boolean adduser(String pseudo, String password, String mail ){
                   
               String sql = "INSERT INTO user(PSEUDO,PASSWORD,MAIL) VALUES(?,?,?)";

               try (Connection conn = this.connect();
                       PreparedStatement pstmt = conn.prepareStatement(sql)) {
                   pstmt.setString(1, pseudo);
                   pstmt.setString(2, password);
                   pstmt.setString(3, mail);
                   pstmt.executeUpdate();
                   return true;
               } catch (SQLException e) {
                   System.out.println(e.getMessage());
                   System.out.println(2);
               }

            return false;
         }




         public boolean login(String pseudo, String password){
                   
            String sql = "SELECT * FROM user where pseudo =? and password = ? ";
            ResultSet resultSet;
            try (Connection conn = this.connect();
                    PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, pseudo);
                pstmt.setString(2, password);
                resultSet = pstmt.executeQuery();

                if (resultSet.next() ){
                    return true;
                }
                else {
                    return false;
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                System.out.println(3);
            }

         return false;
      }



      public boolean changepassword( String password, String pseudo ){
                   
        String sql = "UPDATE USER SET PASSWORD = ? WHERE PSEUDO = ?  ";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, password);
            pstmt.setString(2, pseudo);
            pstmt.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println(4);
        }

     return false;
  }





  public boolean changemail( String mail, String pseudo ){
                   
    String sql = "UPDATE USER SET MAIL = ? WHERE PSEUDO = ?  ";

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setString(1, mail);
        pstmt.setString(2, pseudo);
        pstmt.executeUpdate();
        return true;
    } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.out.println(5);
    }

 return false;
}

public boolean creersondage(String arg1, String arg2, String location, String date, Integer iduser ){
                   
    String sql = "INSERT INTO APPOINTEMENT(ARG1,ARG2,LOCATION,DATE) VALUES(?,?,?,?)";
    String sql2 = "INSERT INTO VOTE(Vote1,Vote2,IDappointement,IDuser) VALUES(0,0,?,?)";
    String sql3 = " SELECT ID FROM APPOINTEMENT WHERE  ID = (SELECT MAX(ID)  FROM APPOINTEMENT) ";
    Integer idApp = null;
    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setString(1, arg1);
        pstmt.setString(2, arg2);
        pstmt.setString(3, location);
        pstmt.setString(4, date);
        pstmt.executeUpdate();
        
    } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.out.println(6);
    }

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql3)) {
        ResultSet rs = pstmt.executeQuery(); 
        rs.next();
        idApp = rs.getInt(1);
        
    } catch (SQLException e) {
        System.out.println(e.getMessage());

    }

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql2)) {
        pstmt.setInt(1, idApp);
        pstmt.setInt(2, iduser);
        pstmt.executeUpdate();
        return true;        
    } catch (SQLException e) {
        System.out.println(e.getMessage());

    }




 return false;
}



public List<Map> sondagelist(){
                   
    String sql = "SELECT APPOINTEMENT.ID AS ID, ARG1, ARG2, LOCATION, DATE, Vote1, Vote2 FROM APPOINTEMENT INNER JOIN VOTE ON APPOINTEMENT.ID = VOTE.ID";
    ResultSet resultSet;
    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {

        resultSet = pstmt.executeQuery();
        List<Map> list = new LinkedList<>();
        while(resultSet.next()){
            Integer id = resultSet.getInt("ID");
            String arg1 = resultSet.getString("ARG1");
            String arg2 = resultSet.getString("ARG2");
            String location = resultSet.getString("LOCATION");
            String date = resultSet.getString("DATE");
            Integer vote1 = resultSet.getInt("Vote1");
            Integer vote2 = resultSet.getInt("Vote2");
            Map<String,Object> map = new HashMap<String,Object>();
            map.put("ID", id);
            map.put("ARG1", arg1);
            map.put("ARG2", arg2);
            map.put("LOCATION", location);
            map.put("DATE", date);
            map.put("Vote1", vote1);
            map.put("Vote2", vote2);
            list.add(map);

        }


        return list;

    } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.out.println(7);
    }

    return null;

}

public boolean vote( Integer vote1, Integer vote2, Integer IDappointement, Integer IDuser ){
                   
    String sql = "UPDATE VOTE SET Vote1 = Vote1 + ?, Vote2 = Vote2 + ? WHERE IDappointement = ? AND IDuser = ?; ";

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setInt(1, vote1);
        pstmt.setInt(2, vote2);
        pstmt.setInt(3, IDappointement);
        pstmt.setInt(4, IDuser);
        System.out.println(vote1+ " "+ vote2 + " " + IDappointement + " " + IDuser);
        pstmt.executeUpdate();
        return true;
    } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.out.println(8);
    }

 return false;
}



public Integer getiduser(String pseudo){
    System.out.println(pseudo);
    String sql = "SELECT ID FROM user where pseudo =? ";
    ResultSet resultSet;
    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setString(1, pseudo);
        resultSet = pstmt.executeQuery();
        return resultSet.getInt("ID");



    } catch (SQLException e) {
        System.out.println(e.getMessage());
        System.out.println(10);
    }
    return null;

}



public boolean deleteuser(String pseudo ){
                   
    String sql = "DELETE FROM USER WHERE PSEUDO = ?  ";

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setString(1, pseudo);
        pstmt.executeUpdate();
        return true;
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }

 return false;
}

public boolean deletesondage(Integer id ){
                   
    String sql = "DELETE FROM APPOINTEMENT WHERE ID = ?  ";

    try (Connection conn = this.connect();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setInt(1, id);
        pstmt.executeUpdate();
        return true;
    } catch (SQLException e) {
        System.out.println(e.getMessage());
    }

 return false;
}

      

}
