package com.example.springboot;

import java.sql.*;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.apache.catalina.connector.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin(origins="*")
public class HelloController {
private bdd bdd;
@PostConstruct
public void init(){
	this.bdd=new bdd();
}

	@RequestMapping("/")
	public String index() {
		return "Bienvenue dans l'API";
	}

	@RequestMapping(value="/api/login" , method=RequestMethod.POST)
	public ResponseEntity<?> login(@RequestBody Map<String,String> json) { 
	if	(this.bdd.login(json.get("username"),json.get("password"))){
		return ResponseEntity.ok().body("User et mdp trouver");	
	}
	return ResponseEntity.status(401).body("Login failed");	
	}


	@RequestMapping(value="/api/registration" , method=RequestMethod.POST)
	public ResponseEntity<?> registration(@RequestBody Map<String,String> json) {
		this.bdd.adduser(json.get("username"),json.get("password"), json.get("mail"));
	return ResponseEntity.ok().body("User correctement inscrit");	
	}

	@RequestMapping(value="/api/myaccount" , method=RequestMethod.PUT)
	public ResponseEntity<?> changepassword(@RequestBody Map<String,String> json) {
		this.bdd.changepassword(json.get("password"),json.get("username"));
	return ResponseEntity.ok().body("password correctement changer");	
	}


	@RequestMapping(value="/api/myaccount" , method=RequestMethod.POST)
	public ResponseEntity<?> changemail(@RequestBody Map<String,String> json) {
		this.bdd.changemail(json.get("mail"),json.get("username"));
	return ResponseEntity.ok().body("mail correctement changer");	
	}



	
	@RequestMapping(value="/api/myaccount" , method=RequestMethod.PATCH)
	public ResponseEntity<?> deleteuser(@RequestBody Map<String,String> json) {
		this.bdd.deleteuser(json.get("username"));
	return ResponseEntity.ok().body("user deleted");	
	}




	@RequestMapping(value="/api/sondage" , method=RequestMethod.POST)
	public ResponseEntity<?> creersondage(@RequestBody Map<String,String> json) {
		this.bdd.creersondage(json.get("arg1"),json.get("arg2"), json.get("location"), json.get("date"),this.bdd.getiduser(json.get("username")) );
	return ResponseEntity.ok().body("Sondage créer");	
	}
	
	
	@RequestMapping(value="/api/sondage", method=RequestMethod.GET )
	public ResponseEntity<?> sondagelist() {
	return ResponseEntity.ok().body(this.bdd.sondagelist());	
	}

	@RequestMapping(value="/api/sondage" , method=RequestMethod.PUT)
	public ResponseEntity<?> vote(@RequestBody Map<String,String> json) {
		this.bdd.vote(Integer.valueOf(json.get("vote1")),Integer.valueOf(json.get("vote2")),Integer.valueOf(json.get("IDappointement")),this.bdd.getiduser(json.get("username")));
	return ResponseEntity.ok().body("Vote pris en compte");	
	}


	@RequestMapping(value="/api/sondage" , method=RequestMethod.PATCH)
	public ResponseEntity<?> deletesondage(@RequestBody Map<String,String> json) {
		this.bdd.deletesondage(Integer.valueOf(json.get("id")));
	return ResponseEntity.ok().body("sondage deleted");	
	}

}
