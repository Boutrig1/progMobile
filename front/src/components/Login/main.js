/* eslint-disable no-console */
export default {
    name: 'Login',
    data: function() {
        return {
            password:"",
            pseudo:"",
        }
    },
    computed: {

    },
    methods: {
        login(evt){
            evt.preventDefault()
            let obj=this
            this.$axios.post("http://localhost:8081/api/login", {username:this.pseudo,password:this.password} ).then ((reponse)=>{
                console.log(reponse)
                obj.$session.start()
                obj.$session.set("username", obj.pseudo)
                obj.$toast.open({
                    message:"Connexion réussie",
                    type:'success',
                    position:"bottom-right"
                })
                obj.$router.push("/")
               // setTimeout(function(){
               //     document.location.reload()
               // }, 3000)
            }).catch((error)=> {
                console.log(error)
                obj.$toast.open({
                    message:"Pseudo ou mot de passe incorrect",
                    type:'error',
                    position:"bottom-right"
                })
            })
        }
    },
    components: {

    }
}