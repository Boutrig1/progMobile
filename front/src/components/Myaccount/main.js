/* eslint-disable no-console */
export default {
    name: 'Myaccount',
    data: function() {
        return {
            password: "",
            Mail: "",
        }
    },
    computed: {

    },
    methods: {
        changepassword(evt){
            let obj=this
            evt.preventDefault()
            this.$axios.put("http://localhost:8081/api/myaccount", {password:this.password, username: obj.$session.get("username")} ).then ((reponse)=>{
                console.log(reponse)
                obj.$toast.open({
                    message:"Changement de mot de passe réussie",
                    type:'success',
                    position:"bottom-right"
                })
            }).catch((error)=> {
                console.log(error)
                obj.$toast.open({
                    message:"Echec du changement de mot de passe ",
                    type:'error',
                    position:"bottom-right"
                })
                
            })
        },


        changemail(evt){
            let obj=this
            evt.preventDefault()
            this.$axios.post("http://localhost:8081/api/myaccount", {mail:this.Mail, username: obj.$session.get("username")} ).then ((reponse)=>{
                console.log(reponse)
                obj.$toast.open({
                    message:"Changement de mail réussie",
                    type:'success',
                    position:"bottom-right"
                })
            }).catch((error)=> {
                console.log(error)
                obj.$toast.open({
                    message:"Echec du changement de mail ",
                    type:'error',
                    position:"bottom-right"
                })
                
            })
        },

        deleteuser(evt){
            let obj=this
            evt.preventDefault()
            this.$axios.patch("http://localhost:8081/api/myaccount", {username: obj.$session.get("username")} ).then ((reponse)=>{

                console.log(reponse)
                obj.$toast.open({
                    message:"Suppression de compte réussie",
                    type:'error',
                    position:"bottom-right"
                })

                this.$session.destroy()
                document.location.reload()
                this.$router.push("/login")

            }).catch((error)=> {
                console.log(error)
                obj.$toast.open({
                    message:"Echec de la suppression de compte",
                    type:'error',
                    position:"bottom-right"
                })
                
            })
        }




    },
    components: {

    }
}