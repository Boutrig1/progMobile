/* eslint-disable no-console */
export default {
    name: 'Sondage',
    data: function() {
        return {
            arg1:"",
            arg2:"",
            location:"",
            date:"",
            sondagelist:[],
        }
    },
    computed: {

    },
    methods: {

        creersondage(evt){
            let obj=this
            evt.preventDefault()
            this.$axios.post("http://localhost:8081/api/sondage", {arg1:this.arg1,arg2:this.arg2, location:this.location, date:this.date, username:obj.$session.get("username")} ).then ((reponse)=>{
                console.log(reponse)
                obj.$toast.open({
                    message:"Sondage créer",
                    type:'success',
                    position:"bottom-right"
                })

            }).catch((error)=> {
                console.log(error)

                obj.$toast.open({
                    message:"Création du sondage a échouer",
                    type:'error',
                    position:"bottom-right"})
            }).then(()=>{
                this.$axios.get("http://localhost:8081/api/sondage").then((response)=>{
                    this.sondagelist=response.data
                })
            })
            this.arg1 = ""
            this.arg2 = ""
            this.location = ""
            this.date = ""
        },

        vote(id,side){
            let obj=this

            if(side===1){
                this.$axios.put("http://localhost:8081/api/sondage", {vote1:1,vote2:0, IDappointement:id, username:obj.$session.get("username")} ).then ((reponse)=>{
                    console.log(reponse)
                    obj.$toast.open({
                        message:"Vote 1 pris en compte",
                        type:'success',
                        position:"bottom-right"
                    })
    
                }).catch((error)=> {
                    console.log(error)
    
                    obj.$toast.open({
                        message:"Le vote 1 a échouer",
                        type:'error',
                        position:"bottom-right"})
                }).then(()=>{
                    this.$axios.get("http://localhost:8081/api/sondage").then((response)=>{
                        this.sondagelist=response.data
                    })
                })
                
            } else {
                this.$axios.put("http://localhost:8081/api/sondage", {vote1:0,vote2:1, IDappointement:id, username:obj.$session.get("username")} ).then ((reponse)=>{
                    console.log(reponse)
                    obj.$toast.open({
                        message:"Vote 2 pris en compte",
                        type:'success',
                        position:"bottom-right"
                    })
    
                }).catch((error)=> {
                    console.log(error)
    
                    obj.$toast.open({
                        message:"Le vote 2 a échouer",
                        type:'error',
                        position:"bottom-right"})
                }).then(()=>{
                    this.$axios.get("http://localhost:8081/api/sondage").then((response)=>{
                        this.sondagelist=response.data
                    })
                })



            }

        },





        deletesondage(id){
            let obj=this
            this.$axios.patch("http://localhost:8081/api/sondage", {id:id} ).then ((reponse)=>{
                console.log(reponse)
                obj.$toast.open({
                    message:"Sondage Supprimer",
                    type:'success',
                    position:"bottom-right"
                })

            }).catch((error)=> {
                console.log(error)

                obj.$toast.open({
                    message:"Suppresion du sondage a échouer",
                    type:'error',
                    position:"bottom-right"})
            }).then(()=>{
                this.$axios.get("http://localhost:8081/api/sondage").then((response)=>{
                    this.sondagelist=response.data
                })
            })


        }


    },
    components: {

    },

    mounted:function(){
        this.$axios.get("http://localhost:8081/api/sondage").then((response)=>{
                    this.sondagelist=response.data
                })
    }
}