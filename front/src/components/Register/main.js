/* eslint-disable no-console */
export default {
    name: 'Register',
    data: function() {
        return {
            password:"",
            pseudo:"",
            mail:"",
        }
    },
    computed: {

    },
    methods: {
        register(evt){
            let obj=this
            evt.preventDefault()
            this.$axios.post("http://localhost:8081/api/registration", {username:this.pseudo,password:this.password, mail:this.mail} ).then ((reponse)=>{
                console.log(reponse)
                obj.$toast.open({
                    message:"Inscription réussie",
                    type:'success',
                    position:"bottom-right"
                })
                this.$router.push("/login")
               // setTimeout(function(){
                //    document.location.reload()
               // }, 3000)
            }).catch((error)=> {
                console.log(error)

                obj.$toast.open({
                    message:"Inscription a échouer essayer un autre pseudo",
                    type:'error',
                    position:"bottom-right"})
            })
        }
    },
    components: {

    }
}