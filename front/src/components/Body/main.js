/* eslint-disable no-console */
export default {
    name: 'Login',
    data: function() {
        return {
            password:"",
            pseudo:"",
            mail:"",
        }
    },
    computed: {

    },
    methods: {
        register(evt){
            evt.preventDefault()
            this.$axios.post("http://localhost:8081/api/registration", {username:this.pseudo,password:this.password, mail:this.mail} ).then ((reponse)=>{
                console.log(reponse)
            }).catch((error)=> {
                console.log(error)
            })
        },

        logout(){
            this.$session.destroy()
            document.location.reload()
            this.$router.push("/")
        }
    },
    components: {

    }
}