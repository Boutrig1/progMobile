import Vue from 'vue'
//import App from './App.vue'
import Body from './components/Body/component.vue'
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import axios from 'axios';
import VueRouter from 'vue-router'
import Register from './components/Register/component.vue'
import Login from './components/Login/component.vue'
import Sondage from './components/Sondage/component.vue'
import Myaccount from './components/Myaccount/component.vue'
import VueSession from 'vue-session'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import Home from './components/Home/component.vue'
import Contact from './components/Contact/component.vue'



Vue.use(VueToast)
Vue.use(VueSession)
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.config.productionTip = false
Vue.prototype.$axios= axios
Vue.prototype.$toast=Vue.$toast


const routes = [
  {
      path: '/register',
      name: 'Register',
      component: Register
  },

  {
    path: '/login',
    name: 'Login',
    component: Login
  },

  {
    path: '/sondage',
    name: 'Sondage',
    component: Sondage
},

  {
    path: '/myaccount',
    name: 'Myaccount',
    component: Myaccount
  },

  {
    path: '/',
    name: 'Home',
    component: Home
},  

{
  path: '/contact',
  name: 'Contact',
  component: Contact
},

]

const router = new VueRouter({
  routes 
})


new Vue({
  router,
  render: h => h(Body),
}).$mount('#app')
